﻿using System;
using System.Text;
using System.Threading.Tasks;
using Akka.Actor;
using Akka.IO;

namespace AkkaIOConsoleHost
{
    class EchoConnection : UntypedActor
    {
        private readonly IActorRef _connection;
        private readonly IActorRef _router;

        public EchoConnection(IActorRef connection, IActorRef router)
        {
            _connection = connection;
            _router = router;
        }

        protected override void OnReceive(object message)
        {
            if (message is Tcp.Received)
            {
                var received = message as Tcp.Received;
                
              
                if (received.Data.Head == 'x')
                    Context.Stop(Self);
                else
                    _connection.Tell(Tcp.Write.Create(received.Data));
            }
            else Unhandled(message);
        }
    }
}