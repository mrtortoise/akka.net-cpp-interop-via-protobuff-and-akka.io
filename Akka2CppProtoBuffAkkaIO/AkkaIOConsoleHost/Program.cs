﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Akka.Actor;
using Akka.Configuration;
using Akka.IO;

namespace AkkaIOConsoleHost
{
    /// <summary>
    /// Code in this project is almost entirley from http://getakka.net/docs/IO
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Console Host starting");
            var config = ConfigurationFactory.ParseString
                (@"
                    akka {
                        actor {
                            provider = ""Akka.Remote.RemoteActorRefProvider, Akka.Remote""
                        }
                    
                            remote {
                            enabled-transports = [""Akka.Remote.Akka-IO""]
                    
                            akka-io {
                                transport-class = ""Akka.Remote.AkkaIOTransport.AkkaIOTransport, Akka.Remote.AkkaIOTransport""
                                hostname = ""127.0.0.1""
                                port = 9098
                            }
                        }
                    }");

            var system = ActorSystem.Create("ioHost");//, config);
            var manager = system.Tcp();

            var server = system.ActorOf(Props.Create(() => new EchoServer(9098)));

            Console.WriteLine("hit any key to exist");
            Console.ReadLine();
        }
    }
}
