﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Akka.Actor;
using Akka.Configuration;
using Akka.IO;
using Akka.Routing;

namespace AkkaIOConsoleClient
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("client starting");
            var config = ConfigurationFactory.ParseString(@"
akka {
    actor {
        provider = ""Akka.Remote.RemoteActorRefProvider, Akka.Remote""
    }

        remote {
        enabled-transports = [""akka.remote.akka-io""]

        akka-io {
            transport-class = ""Akka.Remote.AkkaIOTransport.AkkaIOTransport, Akka.Remote.AkkaIOTransport""
            hostname = ""127.0.0.1""
            port = 9099
        }
    }
}");
            var system = ActorSystem.Create("ioClient");//, config);
            var manager = system.Tcp();

            var client = system.ActorOf(Props.Create(() => new TelnetClient("127.0.0.1", 9098)));


            while (true)
            {
                Thread.Sleep(100);
            }


        }
    }
}
