#Object

1. Use protobuf as a serialisation for cross platform + language communication.
1. (akka) Implement some kind of crude configurable relay to redirect messages to appropiate actors.
1. Implement chunking and reconstructing messages
	- probably going to lean on protobuf specific feature of its streams.
1. Thrash about until figure out what relative complexity of various approaches is.
	- primary goal is something that works over performant or elegant
	- primary end of this project however is performant and elegant whilst being Tell based.

## General Approaches

1. want to avoid a classical servive layer contract
	- by this i mean list of methods with return types.
	- all services are esentially an endpoint, the contract is a convinience that doesn't fit akka.
	- a service implementation *essentially* takse a call, 
		1. figures out what method is being called.
		2. parse everythign else into parameters
		3. holds some caller info to handle the return path from next step
		4. delegate to service implementation which return soemthing
		5. uses stored caller info to cosntruct a call message to send back.
		6. makes the return call
2. need to figure out if someone else is already doign this.

### Technical problems
1. chunking for large messages
	- why chunk?
		- because of concurrency
		- resource hogging
		-  need for thing slike heartbeats.
		-  general responsiveness
	- most messages are goign to be small.
		- so large messages will fit assumptions of rest of system ie availability.
2. generally knowing what messages you are dealing with
3. knowing what stage of a series of messages is currently being processed
	- scenario
		- in middle of a sequence and then get an unexpected type of message how to proceed?
			- drop the sequence?
			- drop the unexpected message?
		- timeouts?


## Solution structure
- common folder
	- proto fodler
		- contains protos to be shared accross projects.
		- the parent folder will contain a batch to compile these into relevant places
	- project here will also contain c# that is shared acccross projects

- c# servers
	- grpc 
		- this will be a grpc service that has 2-3 methods rpc style.
			- straight up method call
			- async
			- something with streams.
	- akki.io 
		- to figure out how to identify message type in a sane way.
			- ideal solution is to figure out how to go from an arbitrary byte[] into an object that akka can match on.
				- problem is that protobuff has nothing obviously inbiuilt as it comes from assumption of receiver knowing somethign about intent of caller.
			- probably solution is to frame protobuf payload with something like
				- message type
				- correlation id
				- whatever auth token
				- ... profit?
- c# clients
	- grpc
		- this will be the client to the above service.
		- will pump some messages make basic assertions abotu responses.