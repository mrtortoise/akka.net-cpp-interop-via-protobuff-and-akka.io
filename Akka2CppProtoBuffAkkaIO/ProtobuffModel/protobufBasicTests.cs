﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Google.Protobuf;
using Google.Protobuf.Examples.AddressBook;
using Google.Protobuf.Reflection;
using NUnit.Framework;

namespace ProtobuffModel
{
    [TestFixture]
    public class protobufBasicTests
    {
        [Test]
        public void BasicSerialiseDeserialiseTest()
        {
            var person = new Person()
            {
                Email = "test@test.com",
                Id = 2,
                Name = "el foo",
                Phones =
                {
                    new Person.Types.PhoneNumber()
                    {
                        Number = "97869786",
                        Type = Person.Types.PhoneType.HOME
                    }
                }
            };

            var bytes = person.ToByteArray();

            var person2 = Person.Parser.ParseFrom(bytes);
           
            Assert.AreEqual(person, person2);
        }
    }
}
